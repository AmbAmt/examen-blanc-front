import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import axios, { AxiosError, AxiosRequestConfig } from 'axios';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));

export const authInterceptor = (): void => {
  console.log("add header")
  axios.interceptors.request.use(
    (config: AxiosRequestConfig) => {
      if (config.headers)
        config.headers['Authorization'] = 'Basic YW1icmU6YW1icmU='
      return config
    },
    (error: AxiosError) => {
      console.error('ERROR:', error)
      Promise.reject(error)
    }
  )
}
authInterceptor(); 