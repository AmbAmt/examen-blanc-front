import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import axios from 'axios';
import { authInterceptor } from 'src/main';
import { Articles } from '../models/app.model';

@Injectable({
  providedIn: 'root'
})
export class ArticlesServiceService {
  private apiUrl: string = "http://localhost:8080/articles";
  public articles$: Articles[] = [];
  public article$?: Articles;

  constructor(private httpClient: HttpClientModule) { }

  public getArticles() {
    axios.get(this.apiUrl)
      .then((response) => {
        this.articles$ = response.data
      }).catch((error) => {
        console.log(error)
      })
  }

  public getArticleByTid(tid: string) {
    axios.get(this.apiUrl + "/" + tid)
      .then((res) => {
        this.article$ = res.data;
        console.log(this.articles$)
      })
      .catch((error) => {
        console.log(error);
      });
  }

  public createArticle(data: any) {
    axios.post(this.apiUrl, {
      "title": data.title,
      "content": data.content,
      "summary": data.summary,
      "tags": data.tags,
      "userEmail": data.userEmail,
    }).then(function (response) {
      console.log(response)
    }).catch(function (error) {
      console.log(error)
    })
    return "Création effectuée";
  }

  public updateArticle(tid: String, data: any) {
    axios.put(this.apiUrl + "/" + tid, {
      "content": data.content,
      "tags": data.tags
    }).then(function (response) {
      console.log(response)
    }).catch(function (error) {
      console.log(error)
    })
    return "Modifications"
  }
}
