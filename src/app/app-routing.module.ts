import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesAddPageComponent } from './pages/articles-add-page/articles-add-page.component';
import { ArticlesDetailPageComponent } from './pages/articles-detail-page/articles-detail-page.component';
import { ArticlesModifyPageComponent } from './pages/articles-modify-page/articles-modify-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'articles/:tid', component: ArticlesDetailPageComponent },
  { path: 'add', component: ArticlesAddPageComponent },
  { path: 'modify/:tid', component: ArticlesModifyPageComponent },
  { path: 'connexion', component: LoginPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
