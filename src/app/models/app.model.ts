export interface Articles {
    tid: String,
    date: Date,
    title: String,
    content: String,
    summary: String,
    tags: Array<String>
    user: Users,
}

export interface Users {
    tid: String,
    userName: String,
    email: String,
    password: String,
}