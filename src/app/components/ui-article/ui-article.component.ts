import { Component, Input, OnInit } from '@angular/core';
import { Articles } from 'src/app/models/app.model';
import { ArticlesServiceService } from 'src/app/services/articles-service.service';

@Component({
  selector: 'app-ui-article',
  templateUrl: './ui-article.component.html',
  styleUrls: ['./ui-article.component.scss']
})
export class UiArticleComponent implements OnInit {

  @Input()
  public article?: Articles

  constructor(public articleService: ArticlesServiceService) { }

  ngOnInit(): void {
  }

}
