import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ArticlesServiceService } from 'src/app/services/articles-service.service';

@Component({
  selector: 'app-articles-modify-page',
  templateUrl: './articles-modify-page.component.html',
  styleUrls: ['./articles-modify-page.component.scss']
})
export class ArticlesModifyPageComponent implements OnInit {

  constructor(public articlesService: ArticlesServiceService, private fb: FormBuilder, private route: ActivatedRoute) { }

  validator: String = "";

  articleForm = this.fb.group({
    content: ['', Validators.required],
  })

  public onSubmit() {
    this.route.params.subscribe(params => {
      this.validator = this.articlesService.updateArticle(params['tid'], this.articleForm.value);
    })

  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.articlesService.getArticleByTid(params['tid']);
    })

    console.log("TEST" + this.articlesService.article$?.title)
  }
}
