import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesModifyPageComponent } from './articles-modify-page.component';

describe('ArticlesModifyPageComponent', () => {
  let component: ArticlesModifyPageComponent;
  let fixture: ComponentFixture<ArticlesModifyPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesModifyPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesModifyPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
