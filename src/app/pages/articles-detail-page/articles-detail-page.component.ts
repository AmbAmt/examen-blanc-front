import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Observable, switchMap, tap } from 'rxjs';
import { Articles } from 'src/app/models/app.model';
import { ArticlesServiceService } from 'src/app/services/articles-service.service';

@Component({
  selector: 'app-articles-detail-page',
  templateUrl: './articles-detail-page.component.html',
  styleUrls: ['./articles-detail-page.component.scss']
})

export class ArticlesDetailPageComponent implements OnInit {
  @Input()
  public article?: Articles;

  constructor(private route: ActivatedRoute, public articleService: ArticlesServiceService) {
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.articleService.getArticleByTid(params['tid']);
    })
  }
};