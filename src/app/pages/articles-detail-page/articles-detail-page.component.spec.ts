import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesDetailPageComponent } from './articles-detail-page.component';

describe('ArticlesDetailPageComponent', () => {
  let component: ArticlesDetailPageComponent;
  let fixture: ComponentFixture<ArticlesDetailPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesDetailPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesDetailPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
