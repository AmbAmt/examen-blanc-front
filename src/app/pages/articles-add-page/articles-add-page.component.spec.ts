import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlesAddPageComponent } from './articles-add-page.component';

describe('ArticlesAddPageComponent', () => {
  let component: ArticlesAddPageComponent;
  let fixture: ComponentFixture<ArticlesAddPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArticlesAddPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlesAddPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
