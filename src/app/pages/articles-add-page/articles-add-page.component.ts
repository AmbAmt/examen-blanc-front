import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ArticlesServiceService } from 'src/app/services/articles-service.service';

@Component({
  selector: 'app-articles-add-page',
  templateUrl: './articles-add-page.component.html',
  styleUrls: ['./articles-add-page.component.scss']
})
export class ArticlesAddPageComponent implements OnInit {

  constructor(public articlesService: ArticlesServiceService, private fb: FormBuilder) { }

  validator: String = "";

  articleForm = this.fb.group({
    title: ['', Validators.required],
    content: ['', Validators.required],
    summary: ['', Validators.required],
    tags: ['', Validators.required],
    userEmail: ['', Validators.required],
  })

  public onSubmit() {
    console.log(this.articleForm.value)
    this.validator = this.articlesService.createArticle(this.articleForm.value);
  }

  ngOnInit(): void {
  }

}
