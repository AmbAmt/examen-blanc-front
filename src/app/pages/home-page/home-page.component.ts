import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ArticlesServiceService } from 'src/app/services/articles-service.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  public isInitialLoading = false;

  constructor(public articleService: ArticlesServiceService, private router: Router) { }

  async ngOnInit() {
    // if (!this.articleService.articles$.length) {
    //this.isInitialLoading = true
    // await 
    this.articleService.getArticles()
    // this.isInitialLoading = false
    // }

  }

}
