import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ArticlesDetailPageComponent } from './pages/articles-detail-page/articles-detail-page.component';
import { ArticlesAddPageComponent } from './pages/articles-add-page/articles-add-page.component';
import { ArticlesModifyPageComponent } from './pages/articles-modify-page/articles-modify-page.component';
import { UiHeaderComponent } from './components/ui-header/ui-header.component';
import { UiArticleComponent } from './components/ui-article/ui-article.component';
import { icons, LucideAngularModule } from 'lucide-angular';
import { HttpClientModule } from '@angular/common/http'
import { ReactiveFormsModule } from '@angular/forms';
import { LoginPageComponent } from './pages/login-page/login-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    ArticlesDetailPageComponent,
    ArticlesAddPageComponent,
    ArticlesModifyPageComponent,
    UiHeaderComponent,
    UiArticleComponent,
    LoginPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LucideAngularModule.pick(icons),
    HttpClientModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
